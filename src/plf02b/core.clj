(ns plf02b.core)

(defn función-associative?-1
  [a]
  (associative? a))
(defn función-associative?-2
  [b]
  (associative? b))
(defn función-associative?-3
  [b]
  (associative? b))
(función-associative?-1 {:becky "gameplays" :juega "Tombraider"})
(función-associative?-2 '([54 9 51 2]))
(función-associative?-3 [1 2 3 4])

(defn función-boolean?-1
  [a]
  (boolean? a))
(defn función-boolean?-2
  [b]
  (boolean? b))
(defn función-boolean?-3
  [d]
  (boolean? d))
(función-boolean?-1 0)
(función-boolean?-2 (== 3 1))
(función-boolean?-3 (- 4 3))

(defn función-char?-1
  [c]
  (char? c))
(defn función-char?-2
  [c]
  (char? c))
(defn función-char?-3
  [c]
  (char? c))
(función-char?-1 \a)
(función-char?-2 (first [\@ 5 true]))
(función-char?-3 6)


(defn función-coll?-1
  [a]
  (coll? a))
(defn función-coll?-2
  [b]
  (coll? b))
(defn función-coll?-3
  [c]
  (coll? c))
(función-coll?-1 [1 9 1])
(función-coll?-2 '(:hola? adios :love amor))
(función-coll?-3 "Quiero ir a la escuela")

(defn función-decimal?-1
  [a]
  (decimal? a))
(defn función-decimal?-2
  [b]
  (decimal? b))
(defn función-decimal?-3
  [c]
  (decimal? c))
(función-decimal?-1 1.0M)
(función-decimal?-2 9999999)
(función-decimal?-3 3.0)

(defn función-float?-1
  [a]
  (float? a))
(defn función-float?-2
  [b]
  (float? b))
(defn función-float?-3
  [c]
  (float? c))
(función-float?-1 4.0)
(función-float?-2 "1")
(función-float?-3 3)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn función-ident?-1
  [a]
  (ident? a))
(defn función-ident?-2
  [b]
  (ident? b))
(defn función-ident?-3
  [c]
  (ident? c))

(función-ident?-1 :hola)
(función-ident?-2 'abc)
(función-ident?-3 465)

(defn función-indexed?-1
  [a]
  (indexed? a))
(defn función-indexed?-2
  [b]
  (indexed? b))
(defn función-indexed?-3
  [c]
  (indexed? c))
(función-indexed?-1 [10 20 30])
(función-indexed?-2 #{:54 "caramelo" :7 "sandia"})
(función-indexed?-3 '(10 100 1000))

(defn función-int?-1
  [a]
  (int? a))
(defn función-int?-2
  [b]
  (int? b))
(defn función-int?-3
  [c]
  (int? c))

(función-int?-1 45)
(función-int?-2 34)
(función-int?-3 56)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn función-integer?-1
  [a]
  (integer? a))
(defn función-integer?-2
  [b]
  (integer? b))
(defn función-integer?-3
  [c]
  (integer? c))

(función-integer?-1 6)
(función-integer?-2 2.0)
(función-integer?-3 78)

(defn función-keyword?-1
  [a]
  (keyword? a))
(defn función-keyword?-2
  [b]
  (keyword? b))
(defn función-keyword?-3
  [c]
  (keyword? c))

(función-keyword?-1 'x)
(función-keyword?-2 :x)
(función-keyword?-3 true)


(defn función-list?-1
  [a]
  (list? a))
(defn función-list?-2
  [a]
  (list? a))
(defn función-list?-3
  [c]
  (list? c))

(función-list?-1 13)
(función-list?-2 '(2 3 4))
(función-list?-3 [1 2 3])

(defn función-map-entry?-1
  [a]
  (map-entry? a))
(defn función-map-entry?-2
  [a]
  (map-entry? a))
(defn función-map-entry?-3
  [c]
  (map-entry? c))

(función-map-entry?-1 :b)
(función-map-entry?-2 (first {:a :b}))
(función-map-entry?-3 [1 2 3 4])
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn función-map?-1
  [a]
  (map? a))
(defn función-map?-2
  [b]
  (map? b))
(defn función-map?-3
  [d]
  (map? d))

(función-map?-1 {3 4})
(función-map?-2 {:a :b})
(función-map?-3 '(1 2 3))

(defn función-nat-int?-1
  [a]
  (nat-int? a))
(defn función-nat-int?-2
  [b]
  (nat-int? b))
(defn función-nat-int?-3
  [c]
  (nat-int? c))

(función-nat-int?-1 0)
(función-nat-int?-2 923)
(función-nat-int?-3 -4)

(defn función-number?-1
  [a]
  (number? a))
(defn función-number?-2
  [b]
  (number? b))
(defn función-number?-3
  [c]
  (map number? c))

(función-number?-1 12)
(función-number?-2 [nil nil])
(función-number?-3 [1 0.4 2])

(defn función-pos-int?-1
  [a]
  (pos-int? a))
(defn función-pos-int?-2
  [b]
  (pos-int? b))
(defn función-pos-int?-3
  [c]
  (pos-int? c))

(función-pos-int?-1 1)
(función-pos-int?-2 -12)
(función-pos-int?-3 107)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn función-ratio?-1
  [a]
  (ratio? a))
(defn función-ratio?-2
  [b]
  (ratio? b))
(defn función-ratio?-3
  [c]
  (ratio? c))

(función-ratio?-1 0.5)
(función-ratio?-2 22/2)
(función-ratio?-3 [1 2 3])

(defn función-rational?-1
  [a]
  (rational? a))
(defn función-rational?-2
  [b]
  (rational? b))
(defn función-rational?-3
  [c]
  (rational? c))

(función-rational?-1 22/7)
(función-rational?-2 1.2)
(función-rational?-3 0.5)

(defn función-seq?-1
  [a]
  (seq? a))
(defn función-seq?-2
  [b]
  (seq? b))
(defn función-seq?-3
  [c]
  (seq? c))
(función-seq?-1 [1])
(función-seq?-2 [range 1 2])
(función-seq?-3 [4 5 6])

(defn función-seqable?-1
  [a]
  (seqable? a))
(defn función-seqable?-2
  [b]
  (seqable? b))
(defn función-seqable?-3
  [c]
  (seqable? c))

(función-seqable?-1 true)
(función-seqable?-2 (int-array [5 4]))
(función-seqable?-3 [4 3 2])
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,
(defn función-sequential?-1
  [a]
  (sequential? a))
(defn función-sequential?-2
  [b]
  (sequential? b))
(defn función-sequential?-3
  [c]
  (sequential? c))

(función-sequential?-1 2)
(función-sequential?-2 (range 2 6))
(función-sequential?-3 [10 11 12])

(defn función-set?-1
  [a]
  (set? a))
(defn función-set?-2
  [b]
  (set? b))
(defn función-set?-3
  [c]
  (set? (sorted-set c)))

(función-set?-1 [1 2 3])
(función-set?-2 (hash-set 3 4))
(función-set?-3 [1 2 3])

(defn función-some?-1
  [a]
  (some? a))
(defn función-some?-2
  [b]
  (some? b))
(defn función-some?-3
  [c]
  (some? c))

(función-some?-1 :kw)
(función-some?-2 [2 3])
(función-some?-3 [10 20 30])
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn función-string?-1
  [a]
  (string? a))
(defn función-string?-2
  [b]
  (string? b))
(defn función-string?-3
  [c]
  (string? c))

(función-string?-1 "baby lemonade")
(función-string?-2 '(15 36))
(función-string?-3 ["a" "b" "c"])

(defn función-symbol?-1
  [a]
  (symbol? a))
(defn función-symbol?-2
  [b]
  (symbol? b))
(defn función-symbol?-3
  [c]
  (symbol? c))

(función-symbol?-1 'a)
(función-symbol?-2 [20 30])
(función-symbol?-3 {:a :b })

(defn función-vector?-1
  [a]
  (vector? a))
(defn función-vector?-2
  [b]
  (vector? b))
(defn función-vector?-3
  [d]
  (vector? d))

(función-vector?-1 '[123456])
(función-vector?-2 '(12))
(función-vector?-3 (hash-set 1 2 3))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;inician funciones de primer orden
(defn función-drop-1
  [a b]
  (drop a b))
(defn función-drop-2
  [a c]
  (drop a c))
(defn función-drop-3
  [a d]
  (drop a d))
(función-drop-1 0 [10 20 30 40])
(función-drop-2 3 [40 50 60 70 90])
(función-drop-3 2 [1000 20000 2000 20])

(defn función-drop-last-1
  [a b]
  (drop-last a b))
(defn función-drop-last-2
  [a c]
  (drop-last a c))
(defn función-drop-last-3
  [a d]
  (drop-last a d))
(función-drop-last-1 0 [200 230 300])
(función-drop-last-2 1 [20 30 40 50])
(función-drop-last-3 3 [1 2 3 4 ])

(defn función-drop-while-1
  [a b]
  (drop-while a b))
(defn función-drop-while-2
  [a c]
  (drop-while a c))
(defn función-drop-while-3
  [a d]
  (drop-while a d))
(función-drop-while-1 neg? [-12 12 12])
(función-drop-while-2 pos? [-1 1.1 1.2])
(función-drop-while-3 zero? [-1 0 1])
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn función-every?-1
  [a]
  (every? even? a))
(defn función-every?-2
  [a b]
  (every? a b))
(defn función-every?-3
  [c d]
  (every? c d))

(función-every?-1 '(2 4 6))
(función-every?-2 #{1 2} [1 2 3])
(función-every?-3 #{1 2} [1 2])

(defn función-filterv-1
  [a]
  (filterv even? a))
(defn función-filterv-2
  [b]
  (filterv even? b))
(defn función-filterv-3
  [c]
  (filterv even? c))

(función-filterv-1 (range 10))
(función-filterv-2 (range 2 6))
(función-filterv-3 (range 10 20 30))

(defn función-group-by-1
  [a b]
  (group-by  a b))
(defn función-group-by-2
  [a b]
  (group-by a b))
(defn función-group-by-3
  [c d]
  (group-by  c d ))

(función-group-by-1 set ["meat" "mat" "team" "mate" "eat" "tea"])
(función-group-by-2 count ["a" "as" "asd" "aa" "asdf" "qwer"])
(función-group-by-3 odd? (range 20))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn función-iterate-1
  [a b]
  (iterate a b))
(defn función-iterate-2
  [a b]
  (iterate a b))
(defn función-iterate-3
  [a c]
  (iterate a c ))

(función-iterate-1 inc 5)
(función-iterate-2 indexed? [8 9 10])
(función-iterate-3 list? (range 5))


(defn función-keep-1
  [a c]
  (keep a c))
(defn función-keep-2
  [a b]
  (keep a b))
(defn función-keep-3
  [a d]
  (keep a d))
(función-keep-1 ratio? [true 1.5 0.444])
(función-keep-2 odd? (range 500 600))
(función-keep-3 rational? '(1/4 5/30))

(defn función-keep-indexed-1
  [a]
  (keep-indexed a))
(defn función-keep-indexed-2
  [a b]
  (keep-indexed a b))
(defn función-keep-indexed-3
  [a c]
  (keep-indexed a c))
(función-keep-indexed-1 #(if (odd? %1) %2))
(función-keep-indexed-2 #(if (pos? %1) %2) [:a :b :c :d :e])
(función-keep-indexed-3 #(if (pos? %2) %1) [5 9.2 5 7 11])

(defn función-map-indexed-1
  [a b]
  (map-indexed a b))
(defn función-map-indexed-2
  [a c]
  (map-indexed a c))
(defn función-map-indexed-3
  [a d]
  (map-indexed a d))
(función-map-indexed-1 vector "quiero dormir")
(función-map-indexed-2 hash-map [3 3 1 2])
(función-map-indexed-3 list "un ratito")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn función-mapcat-1
  [a b]
  (mapcat a b))
(defn función-mapcat-2
  [a b]
  (mapcat a b))
(defn función-mapcat-3
  [a c]
  (mapcat a c))

(función-mapcat-1 reverse [[3 2 1 0] [6 5 4] [9 8 7]])
(función-mapcat-2 reverse (vector (range 5 10)))
(función-mapcat-3 #(remove even? %) [[1 2] [2 2] [2 3]])


(defn función-mapv-1
  [a b]
  (mapv a b))
(defn función-mapv-2
  [a b]
  (mapv + a b))
(defn función-mapv-3
  [a b c]
  (mapv  a b c))

(función-mapv-1 inc [ 1 2 3 4 5])
(función-mapv-2 [1 2 3] [4 5 6])
(función-mapv-3 vector [:a :b :c][:b :c :p])

(defn función-merge-with-1
  [a b]
  (merge-with + a b))
(defn función-merge-with-2
  [a b c]
  (merge a b c))
(defn función-merge-with-3
  [c]
  (merge-with c))

(función-merge-with-1  {:a 1  :b 2} {:a 9  :b 98 :c 0})
(función-merge-with-2 {:a 1} {:a 2} {:a 3})
(función-merge-with-3  [{:a 1  :b 2}
                       {:a 9  :b 98  :c 0}
                       {:a 10 :b 100 :c 10}
                       {:a 5}])
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn función-not-any?-1
  [a b]
  (not-any? a b))
(defn función-not-any?-2
  [ a b]
  (not-any? a b))
(defn función-not-any?-3
  [a c]
  (not-any? a c))

(función-not-any?-1 odd? '(2 4 6))
(función-not-any?-2 odd? '(1 2 3))
(función-not-any?-3 odd? '( 12 13 16))

(defn función-not-every?-1
  [a b]
  (not-every? a b))
(defn función-not-every?-2
  [ a b]
  (not-every? a b))
(defn función-not-every?-3
  [a c]
  (not-every?  a c))

(función-not-every?-1 odd? '(1 2 3))
(función-not-every?-2 odd? '(1 3))
(función-not-every?-3  odd? [1 2 3])

(defn función-partitionby-1
  [a b]
  (partition-by a b))
(defn función-partitionby-2
  [a c]
  (partition-by a c))
(defn función-partitionby-3
  [a d]
  (partition-by a d))

(función-partitionby-1 identity [1 1 1 1 1 2 2 2 3 4 4])
(función-partitionby-2 count ["a" "b" "ab" "ac" "c"])
(función-partitionby-3 #(= 3 %) [1 2 3 4 5])
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



(defn función-remove-1
  [a b]
  (remove a b))
(defn función-remove-2
  [a b]
  (remove a b))
(defn función-remove-3
  [a b ]
  (remove a b ))

(función-remove-1 even? (range 20))
(función-remove-2 #{:a} #{:b :c :d :a :e})
(función-remove-3 #(zero? (mod % 3)) (range 2 6))

(defn función-reverse-1
  [a]
  (reverse a))
(defn función-reverse-2
  [b]
  (reverse b))
(defn función-reverse-3
  [a]
  (reverse a))

(función-reverse-1 '(1 2 3 4))
(función-reverse-2 [3 4 12 12])
(función-reverse-3 "leinnen")


(defn función-some-1
  [a b]
  (some a b))
(defn función-some-2
  [a c]
  (some a c))
(defn función-some-3
  [a d]
  (some  a d))

(función-some-1 even? '(1 2 3 4))
(función-some-2 {2 "dos" 3 nil} [nil 3 2])
(función-some-3  true? [false false false])
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn función-sort-by-1
  [a b]
  (sort-by  a b))
(defn función-sort-by-2
  [a b]
  (sort-by a b))
(defn función-sort-by-3
  [a c]
  (sort-by  a c))

(función-sort-by-1 count ["aaa" "bbbb" "c"])
(función-sort-by-2 count [{:bar 3} {:baz 5}])
(función-sort-by-3 count [{:bar 3} {:baz 5}])

(defn función-split-with-1
  [a b]
  (split-with  a b))
(defn función-split-with-2
  [a b]
  split-with a b)
(defn función-split-with-3
  [a c]
  (split-with a c))

(función-split-with-1 odd? [1 3 5 6 7 9])
(función-split-with-2 3 [1 2 3 4 5])
(función-split-with-3 odd? [12 17 19 18])

(defn función-take-1
  [a b]
  (take a b))
(defn función-take-2
  [a b]
  (take a b))
(defn función-take-3
  [a c]
  (take a c))

(función-take-1 3 '(1 3 5 6))
(función-take-2 4 [5 6 7 8 9 5])
(función-take-3 3 (drop 2 (range 5 10)))

(defn función-take-last-1
  [a b]
  (take-last a b))
(defn función-take-last-2
  [a c]
  (take-last a c))
(defn función-take-last-3
  [a d]
  (take-last a d))

(función-take-last-1 3 [1 2 5 6 4])
(función-take-last-2 2 nil)
(función-take-last-3 -1 [1])

(defn función-take-nth-1
  [a b]
  (take-nth a b))
(defn función-take-nth-2
  [a b ]
  (take a b ))
(defn función-take-nth-3
  [a c]
  (take-nth a c))

(función-take-nth-1 2 (range 10))
(función-take-nth-2 2 (take-nth 3 (range 10)))
(función-take-nth-3 3 (take-nth -10 (range 6)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn función-take-while-1
  [a b]
  (take-while a b))
(defn función-take-while-2
  [a b]
  (take-while a b))
(defn función-take-while-3
  [a c]
  (take-while a c))

(función-take-while-1 neg? [-2 -1 0 1 2 -3])
(función-take-while-2 #{[1 2] [3 4]} #{[4 5]})
(función-take-while-3 neg? [1000 0 -2 -3])

(defn función-update-1
  [a b c]
  (update a b c))

(defn función-update-2
  [ab c d e]
  (update ab c d e))

(función-update-1 {:name "Paulina" :age 26} :age inc)
(función-update-2 [1 2 3 4 5 6 7] 2 + 15)
(función-update-1 {:name "Rebeca" :age 26 :Peso 58} :Peso decimal?)

(defn función-update-in-1
  [a b c]
  (update-in a b c))
(defn función-update-in-2
  [a c d]
  (update-in a c d))
(defn función-update-in-3
  [a b c]
  (update-in a b c))

(función-update-in-1 [1 {:a 2 :b 3 :c 4}] [1 :c] (fnil inc 5))
(función-update-in-2 [1 {:a 2 :b 3 :c 4}] [1 :c] (fnil inc 5))
(función-update-in-3 [1 {:a 2 :b 3 :c 4}] [1 :c] (fnil inc 5))


